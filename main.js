let Animal = (function () {

    let Animal = function (motionType) {
        // Публичное свойство
        this.name = "Animal";
        this.motionType = motionType;
        this.distance = 0;
    };

    // Публичный метод
    Animal.prototype.go = function (distance) {
        this.distance += distance;
        alert(this.name + ' ' + this.motionType + ' ' + this.distance);
    };

    // Экспорт публичной функции
    return Animal;
})();

let Mammalia = (function () {

    let Mammalia = function (motionType, voice) {
        this.name = "Mammalia";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Mammalia.prototype = Object.create(Animal.prototype);
    Mammalia.prototype.constructor = Mammalia;

    // Публичный метод
    Mammalia.prototype.say = function () {
        alert(this.name + ' сказал ' + this.voice);
    };

    // Экспорт публичной функции
    return Mammalia;
})();

let Dog = (function () {

    let Dog = function (motionType, voice) {
        this.name = "Dog";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Dog.prototype = Object.create(Mammalia.prototype);
    Dog.prototype.constructor = Dog;

    // Экспорт публичной функции
    return Dog;
})();

let Fox = (function () {

    let Fox = function (motionType, voice) {
        this.name = "Fox";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Fox.prototype = Object.create(Dog.prototype);
    Fox.prototype.constructor = Fox;

    // Экспорт публичной функции
    return Fox;
})();

let FoxOrange = (function () {

    let FoxOrange = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "тяф";
    };

    //наследование
    FoxOrange.prototype = Object.create(Fox.prototype);
    FoxOrange.prototype.constructor = FoxOrange;

    // Экспорт публичной функции
    return FoxOrange;
})();

let FoxSilver = (function () {

    let FoxSilver = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "тяф";
    };

    //наследование
    FoxSilver.prototype = Object.create(Fox.prototype);
    FoxSilver.prototype.constructor = FoxSilver;

    // Экспорт публичной функции
    return FoxSilver;
})();

let Wolf = (function () {

    let Wolf = function (motionType, voice) {
        this.name = "Wolf";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Wolf.prototype = Object.create(Dog.prototype);
    Wolf.prototype.constructor = Wolf;

    // Экспорт публичной функции
    return Wolf;
})();

let WolfGray = (function () {

    let WolfGray = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "ауу";
    };

    //наследование
    WolfGray.prototype = Object.create(Wolf.prototype);
    WolfGray.prototype.constructor = WolfGray;

    // Экспорт публичной функции
    return WolfGray;
})();

let WolfTundra = (function () {

    let WolfTundra = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "ауу";
    };

    //наследование
    WolfTundra.prototype = Object.create(Wolf.prototype);
    WolfTundra.prototype.constructor = WolfTundra;

    // Экспорт публичной функции
    return WolfTundra;
})();

let Cats = (function () {
    let Cats = function (motionType, voice) {
        this.name = "Cats";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Cats.prototype = Object.create(Mammalia.prototype);
    Cats.prototype.constructor = Cats;

    // Экспорт публичной функции
    return Cats;
})();

let Tiger = (function () {
    let Tiger = function (motionType, voice) {
        this.name = "Tiger";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Tiger.prototype = Object.create(Cats.prototype);
    Tiger.prototype.constructor = Tiger;

    // Экспорт публичной функции
    return Tiger;
})();

let TigerAmur = (function () {
    let TigerAmur = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "ррр";
    };

    //наследование
    TigerAmur.prototype = Object.create(Tiger.prototype);
    TigerAmur.prototype.constructor = TigerAmur;

    // Экспорт публичной функции
    return TigerAmur;
})();

let TigerBengal = (function () {
    let TigerBengal = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "ррр";
    };

    //наследование
    TigerBengal.prototype = Object.create(Tiger.prototype);
    TigerBengal.prototype.constructor = TigerBengal;

    // Экспорт публичной функции
    return TigerBengal;
})();

let Cat = (function () {
    let Cat = function (motionType, voice) {
        this.name = "Cat";
        this.motionType = motionType;
        this.distance = 0;
        this.voice = voice;
    };

    //наследование
    Cat.prototype = Object.create(Cats.prototype);
    Cat.prototype.constructor = Cat;

    // Экспорт публичной функции
    return Cat;
})();

let CatMaineCoon = (function () {
    let CatMaineCoon = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "мяу";
    };

    //наследование
    CatMaineCoon.prototype = Object.create(Cat.prototype);
    CatMaineCoon.prototype.constructor = CatMaineCoon;

    // Экспорт публичной функции
    return CatMaineCoon;
})();

let CatPersian = (function () {
    let CatPersian = function (name, motionType) {
        this.name = name;
        this.motionType = motionType;
        this.distance = 0;
        this.voice = "мяу";
    };

    //наследование
    CatPersian.prototype = Object.create(Cat.prototype);
    CatPersian.prototype.constructor = CatPersian;

    // Экспорт публичной функции
    return CatPersian;
})();

let Factory = (function () {

    let Factory = function () {
    };

    // Публичный метод
    Factory.prototype.createFoxOrange = function (name, motionType) {
        return new FoxOrange(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createFoxSilver = function (name, motionType) {
        return new FoxSilver(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createWolfGrey = function (name, motionType) {
        return new WolfGray(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createWolfTundra = function (name, motionType) {
        return new WolfTundra(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createTigerAmur = function (name, motionType) {
        return new TigerAmur(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createTigerBengal = function (name, motionType) {
        return new TigerBengal(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createCatMaineCoon = function (name, motionType) {
        return new CatMaineCoon(name, motionType);
    };

    // Публичный метод
    Factory.prototype.createCatPersian = function (name, motionType) {
        return new CatPersian(name, motionType);
    };

    // Экспорт публичной функции
    return Factory;
})();

let AnimalsTree = (function () {

    let AnimalsTree = function () {
        this.map = [];
    };

    AnimalsTree.prototype.addAnimal = function (childObj, obj) {
        let parentObj = Object.getPrototypeOf(obj);
        let parentName = parentObj.constructor.name;

        if (parentName != "Animal") {
            if (this.indexOfAnimalsMap(parentName)) {
                if (childObj != null) {
                    this.addNode(parentName, childObj);
                }
            } else {
                if (childObj != null) {
                    this.addNode(parentName, childObj);
                }
                this.addAnimal(obj, parentObj);
            }
        } else {
            this.addNode(parentName, childObj);
        }
    };

    AnimalsTree.prototype.addNode = function (parentName, obj) {
        let node = {};
        node["parentName"] = parentName;
        node["childObjects"] = obj;
        this.map.push(node);
    };

    AnimalsTree.prototype.indexOfAnimalsMap = function (parentName) {
        for (let i = 0; i < this.map.length; i++) {
            if (this.map[i].parentName == parentName) {
                return true;
            }
        }
        return false;
    };

    AnimalsTree.prototype.showTree = function (parentName, white, obj) {
        this.createRadioButton(parentName, white + parentName + "(" + this.getAnimalCount(parentName) + ")", obj);
        for (let i = 0; i < this.map.length; i++) {
            if (this.map[i].parentName == parentName) {
                this.showTree(Object.getPrototypeOf(this.map[i].childObjects).constructor.name, white + "______", this.map[i].childObjects);
            }
        }
    };

    AnimalsTree.prototype.createRadioButton = function (value, name, obj) {
        let animalContainer = document.getElementById("animals_tree_container");
        let h1 = document.createElement('h1');
        let p = document.createElement('p');

        p.appendChild(h1);
        if (this.getAnimalCount(value) == 0) {
            this.createCard(p, name, obj);
        } else {
            let radioInputNode = document.createTextNode(name);
            p.appendChild(radioInputNode);
        }

        animalContainer.appendChild(p);
    };

    AnimalsTree.prototype.getAnimalCount = function (name) {
        let count = 0;
        for (let i = 0; i < this.map.length; i++) {
            if (this.map[i].parentName == name) {
                count++;
            }
        }
        return count;
    };

    AnimalsTree.prototype.createCard = function (container, name, animalObj) {

        let animalContainer = document.getElementById("animals_tree_container");

        let type = document.createTextNode(name);
        let animalName = document.createTextNode("  Name:" + animalObj.name + " ");

        let btnGo = document.createElement('input');
        btnGo.type = "button";
        btnGo.className = "btn";
        btnGo.value = "Go";
        btnGo.onclick = function () {
            animalObj.go(10);
        };

        let btnSay = document.createElement('input');
        btnSay.type = "button";
        btnSay.className = "btn";
        btnSay.value = "Say";
        btnSay.onclick = function () {
            animalObj.say();
        };

        container.appendChild(type);
        container.appendChild(animalName);
        container.appendChild(btnGo);
        container.appendChild(btnSay);

        animalContainer.appendChild(container);
    };

    return AnimalsTree;
})();

window.onload = function () {
    let factory = new Factory();
    let treeCreator = new AnimalsTree();

    treeCreator.addAnimal(null, factory.createCatMaineCoon("Васька", "пробежал"));
    treeCreator.addAnimal(null, factory.createCatPersian("Барсик", "прополз"));
    treeCreator.addAnimal(null, factory.createCatPersian("Мурзик", "прополз"));
    treeCreator.addAnimal(null, factory.createFoxOrange("Алиса", "пробежала"));

    treeCreator.addAnimal(null, factory.createFoxSilver("Лиса", "пробежала"));
    treeCreator.addAnimal(null, factory.createTigerAmur("Амур", "прополз"));
    treeCreator.addAnimal(null, factory.createTigerBengal("Тимур", "пробежал"));

    treeCreator.addAnimal(null, factory.createWolfGrey("Виктор", "пробежала"));
    treeCreator.addAnimal(null, factory.createWolfTundra("Света", "проползла"));

    treeCreator.showTree("Animal", "");
};

